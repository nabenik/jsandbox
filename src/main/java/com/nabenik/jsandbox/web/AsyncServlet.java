package com.nabenik.jsandbox.web;

import java.io.IOException;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(asyncSupported = true, urlPatterns = { "/asyncservlet" })
public class AsyncServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	int instanceVariable = 1;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AsyncContext aCtx = request.startAsync();
		// aCtx.setTimeout(5000);
		aCtx.start(new Runnable() {

			@Override
			public void run() {

				String sleep = request.getParameter("sleep");
				if (sleep != null) {
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				try {
					aCtx.getResponse().getWriter().write("Request Processed");
					System.out.println("Print instance variable#####" + instanceVariable);
					instanceVariable += 1;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				aCtx.complete();
				// aCtx.dispatch("/result.jsp");

			}
		});
	}

}