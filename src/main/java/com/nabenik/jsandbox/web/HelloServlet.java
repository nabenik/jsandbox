package com.nabenik.jsandbox.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "HelloServlet", urlPatterns = {"/hello"}, initParams={@WebInitParam(name="user", value="MP")})
public class HelloServlet extends HttpServlet {
    String user = null;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        user = config.getInitParameter("user");
    }
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        //Init params
        user = request.getParameter("name") == null?user:request.getParameter("name");
        
        //Context
        String config = request.getServletContext().getSessionCookieConfig().getPath();
        
        try (PrintWriter out = response.getWriter()) {
            out.println("<html><head>");
            out.println("<title>Hello Servlet</title>");
            out.println("</head><body>");
            out.println("<h1>My Hello servlet</h1>");
            out.println("<h1>Param</h1>");
            out.println(user);
            out.println("<h1>Context</h1>");
            out.println(config);
            out.println("</body></html>");
        } finally {
        }
        
    }
}
